package leetcode.common;

/**
 * @Description ListNode
 * @Author ccy
 * @Date 2019/5/5 10:01
 */
public class ListNode {
    public int val;

    public ListNode next;

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int[] vals){
        if(vals==null||vals.length==0){
            throw new IllegalArgumentException("no no no");
        }
        ListNode curr =this;
        curr.val = vals[0];
        for(int i=1;i<vals.length;i++){
            curr.next = new ListNode(vals[i]);
            curr = curr.next;
        }
    }

    @Override
    public String toString(){
        StringBuffer s = new StringBuffer("");
        ListNode curNode = this;
        while(curNode != null){
            s.append(Integer.toString(curNode.val));
            s.append("->");
            curNode = curNode.next;
        }
        return s.toString();
    }
}
