package leetcode.common;

/**
 * @Description TODO
 * @Author ccy
 * @Date 2019/5/6 11:14
 */
public class TreeNode {

    public int val;

    public TreeNode left;

    public TreeNode right;

    public TreeNode(int x){val = x;}

    public TreeNode(int x, int left ,int right){
        val = x;
        this.left = new TreeNode(left);
        this.right = new TreeNode(right);
    }

}
