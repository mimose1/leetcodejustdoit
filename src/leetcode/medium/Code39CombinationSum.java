package leetcode.medium;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * 给定一个无重复元素的数组 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。
 * candidates 中的数字可以无限制重复被选取。
 * 说明：
 * 所有数字（包括 target）都是正整数。
 * 解集不能包含重复的组合。
 *
 * Given a set of candidate numbers (candidates) (without duplicates) and a target number (target), find all unique combinations in candidates where the candidate numbers sums to target.
 * The same repeated number may be chosen from candidates unlimited number of times.
 * Note:
 * All numbers (including target) will be positive integers.
 * The solution set must not contain duplicate combinations.
 * @Author ccy
 * @Date 2019/5/5 14:25
 */
public class Code39CombinationSum {

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> temp = new ArrayList<>();
        search(res,temp,candidates,target,0);
        return res;
    }

    private void search(List<List<Integer>> res, List<Integer> temp, int[] candidates, int target, int index){
        for(int i=index; i<candidates.length; i++){
            if(target-candidates[i]==0){
                temp.add(candidates[i]);
                res.add(new ArrayList<>(temp));
                temp.remove(temp.size()-1);
            }else if(target-candidates[i]>0){
                temp.add(candidates[i]);
                search(res, temp, candidates, target-candidates[i], i);
                temp.remove(temp.size()-1);
            }
        }
    }

    public static void main(String[] args) {
        int[] candidates = {2,3,6,7};
        int target = 7;
        List<List<Integer>> list = (new Code39CombinationSum()).combinationSum(candidates, target);
        System.out.println(list.toString());
    }

}
