package leetcode.simple;

import leetcode.common.TreeNode;
import sun.reflect.generics.tree.Tree;

/**
 * @Description
 * 给定两个二叉树，编写一个函数来检验它们是否相同。
 *
 * 如果两个树在结构上相同，并且节点具有相同的值，则认为它们是相同的。
 *
 * Given two binary trees, write a function to check if they are the same or not.
 *
 * Two binary trees are considered the same if they are structurally identical and the nodes have the same value.
 * @Author ccy
 * @Date 2019/5/6 11:19
 */
public class Code100SameTree {
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if(p==null&&q==null) return true;
        if(p!=null&&q!=null&&p.val==q.val) return isSameTree(p.left, q.left)&&isSameTree(p.right, q.right);
        else return false;
    }

    public static void main(String[] args) {
        TreeNode q = new TreeNode(1, 2, 3);
        TreeNode p = new TreeNode(1, 2, 3);
        System.out.println((new Code100SameTree()).isSameTree(p, q));
    }
}
