package leetcode.simple;

import leetcode.common.TreeNode;

import java.util.Stack;

/**
 * @Description
 * 给定一个二叉树，检查它是否是镜像对称的。
 * Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
 *
 * 例如，二叉树 [1,2,2,3,4,4,3] 是对称的。
 * For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
 *
 *     1
 *    / \
 *   2   2
 *  / \ / \
 * 3  4 4  3
 * 但是下面这个 [1,2,2,null,3,null,3] 则不是镜像对称的:
 * But the following [1,2,2,null,3,null,3] is not:
 *
 *     1
 *    / \
 *   2   2
 *    \   \
 *    3    3
 * 说明:
 *
 * 如果你可以运用递归和迭代两种方法解决这个问题，会很加分。
 *
 * Note:
 * Bonus points if you could solve it both recursively and iteratively.
 *
 * @Author ccy
 * @Date 2019/5/6 11:28
 */
public class Code101SymmetricTree {

    public boolean isSymmetric(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        stack.push(root);
        while(!stack.isEmpty()){
            TreeNode t1 = stack.pop();
            TreeNode t2 = stack.pop();
            if(t1==null&&t2==null) continue;
            if(t1==null||t2==null) return false;
            if(t1.val != t2.val) return false;
            stack.push(t1.left);
            stack.push(t2.right);
            stack.push(t1.right);
            stack.push(t2.left);
        }
        return true;
    }

    public boolean isSymmetric2(TreeNode root) {
        return isMirror(root, root);
    }

    private boolean isMirror(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) return true;
        if (t1 == null || t2 == null) return false;
        return (t1.val == t2.val)
                && isMirror(t1.right, t2.left)
                && isMirror(t1.left, t2.right);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2,3,4);
        root.right = new TreeNode(2,4,3);
        root.left.left = new TreeNode(3,1,2);
        root.left.right = new TreeNode(4,1,2);
        root.right.left = new TreeNode(4,2,1);
        root.right.right = new TreeNode(3,2,1);
        System.out.println((new Code101SymmetricTree()).isSymmetric(root));
    }
}
