package leetcode.simple;

import leetcode.common.TreeNode;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Stack;

/**
 * @Description
 * 给定一个二叉树，找出其最大深度。
 *
 * 二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。
 *
 * 说明: 叶子节点是指没有子节点的节点。
 *
 * Given a binary tree, find its maximum depth.
 *
 * The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
 *
 * Note: A leaf is a node with no children.
 * @Author ccy
 * @Date 2019/5/6 14:34
 */
public class Code103MaximumDepthOfBinaryTree {
    public int maxDepth(TreeNode root) {
        //递归
        if(root==null) return 0;
        int length = Math.max(maxDepth(root.left), maxDepth(root.right));
        return length+1;
    }

    public int maxDepth2(TreeNode root){
        //队列_____提交发现效率不高
        if(root==null) return 0;
        Stack<Map.Entry<TreeNode,Integer>> stack = new Stack<>();
        int depth = 1;
        stack.push(new AbstractMap.SimpleEntry<>(root, depth));
        while(!stack.isEmpty()){
            Map.Entry<TreeNode,Integer> entry = stack.pop();
            root = entry.getKey();
            if(root!=null){
                int h = entry.getValue();
                depth = Math.max(depth, h);
                stack.push(new AbstractMap.SimpleEntry<>(root.left, h+1));
                stack.push(new AbstractMap.SimpleEntry<>(root.right, h+1));
            }
        }
        return depth;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2,3,4);
        root.right = new TreeNode(2,4,3);
        root.right.left = new TreeNode(4,2,1);
        System.out.println((new Code103MaximumDepthOfBinaryTree()).maxDepth2(root));
    }
}
