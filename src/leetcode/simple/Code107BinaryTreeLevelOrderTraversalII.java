package leetcode.simple;

import leetcode.common.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Description
 * 给定一个二叉树，返回其节点值自底向上的层次遍历。 （即按从叶子节点所在层到根节点所在的层，逐层从左向右遍历）
 * Given a binary tree, return the bottom-up level order traversal of its nodes' values. (ie, from left to right, level by level from leaf to root).
 *
 * 例如：
 * For example:
 * 给定二叉树 [3,9,20,null,null,15,7],
 * Given binary tree [3,9,20,null,null,15,7],
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * 返回其自底向上的层次遍历为：
 * return its bottom-up level order traversal as:
 * [
 *   [15,7],
 *   [9,20],
 *   [3]
 * ]
 * @Author ccy
 * @Date 2019/5/6 15:18
 */
public class Code107BinaryTreeLevelOrderTraversalII {

    public List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        traversalII(res,root,0);
        Collections.reverse(res);
        return res;
    }

    private void traversalII(List<List<Integer>> res, TreeNode root, int depth) {
        if(root==null) return ;
        if(res.size()==depth) res.add(new ArrayList<>());
        List<Integer> list = res.get(depth);
        list.add(root.val);
        traversalII(res, root.left, depth+1);
        traversalII(res, root.right, depth+1);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2,3,4);
        root.right = new TreeNode(2,4,3);
        root.right.left = new TreeNode(4,2,1);
        List<List<Integer>> list = (new Code107BinaryTreeLevelOrderTraversalII()).levelOrderBottom(root);
        System.out.println(list.toString());
    }
}
