package leetcode.simple;

import leetcode.common.TreeNode;

/**
 * @Description
 * 将一个按照升序排列的有序数组，转换为一棵高度平衡二叉搜索树。
 *
 * 本题中，一个高度平衡二叉树是指一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过 1。
 *
 * Given an array where elements are sorted in ascending order, convert it to a height balanced BST.
 *
 * For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.
 * @Author ccy
 * @Date 2019/5/6 15:53
 */
public class Code108ConvertSortedArrayToBinarySearchTree {
    public TreeNode sortedArrayToBST(int[] nums) {
        if(nums==null) return null;
        int left = 0,right = nums.length;
        return convert(nums,left,right-1);
    }

    private TreeNode convert(int[] nums, int left, int right) {
        if(left>right)
            return null;
        int mid = left+(right-left)/2;
        TreeNode tree = new TreeNode(nums[mid]);
        tree.left = convert(nums,left,mid-1);
        tree.right = convert(nums,mid+1,right);
        return tree;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{-10,-3,0,5,9};
        TreeNode root = (new Code108ConvertSortedArrayToBinarySearchTree().sortedArrayToBST(nums));
        System.out.println("done");
    }
}
