package leetcode.simple;

import com.sun.org.apache.bcel.internal.classfile.Code;
import leetcode.common.TreeNode;

/**
 * @Description
 *给定一个二叉树，判断它是否是高度平衡的二叉树。
 *
 * 本题中，一棵高度平衡二叉树定义为：
 *
 * 一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过1。
 *
 *
 * Given a binary tree, determine if it is height-balanced.
 *
 * For this problem, a height-balanced binary tree is defined as:
 *
 * a binary tree in which the depth of the two subtrees of every node never differ by more than 1.
 * @Author ccy
 * @Date 2019/5/6 16:27
 */
public class Code110BalancedBinaryTree {
    public boolean isBalanced(TreeNode root) {
        if(root==null) return true;
        if(Math.abs(depth(root.left)-depth(root.right))>1) return false;
        return isBalanced(root.left)&&isBalanced(root.right);
    }

    private int depth(TreeNode tree) {
        if(tree==null) return 0;
        return Math.max(depth(tree.left)+1, depth(tree.right)+1);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1,2,2);
        root.left.left = new TreeNode(3);
        root.right.right = new TreeNode(3);
        root.left.left.left = new TreeNode(4);
        root.right.right.right = new TreeNode(4);
        System.out.println((new Code110BalancedBinaryTree()).isBalanced(root));
    }
}
