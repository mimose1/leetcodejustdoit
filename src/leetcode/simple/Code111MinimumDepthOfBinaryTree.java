package leetcode.simple;

import leetcode.common.TreeNode;

/**
 * @Description
 * 给定一个二叉树，找出其最小深度。
 *
 * 最小深度是从根节点到最近叶子节点的最短路径上的节点数量。
 *
 * 说明: 叶子节点是指没有子节点的节点。
 *
 * Given a binary tree, find its minimum depth.
 *
 * The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.
 *
 * Note: A leaf is a node with no children.
 * @Author ccy
 * @Date 2019/5/6 17:14
 */
public class Code111MinimumDepthOfBinaryTree {
    public int minDepth(TreeNode root) {
        if(root==null) return 0;
        int left = minDepth(root.left);
        int right = minDepth(root.right);
        //会发现 [1,2]这种情况，leetCode的预期结果是2，理解：一定要由叶节点结尾才能作为路径，所以该树的最短路径不是1而是2
        return (left>0&&right>0)?Math.min(left, right)+1:left+right+1;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2,3,4);
        root.right = new TreeNode(2,4,3);
        root.right.left = new TreeNode(4,2,1);
//        root.left = new TreeNode(2);
        System.out.println((new Code111MinimumDepthOfBinaryTree()).minDepth(root));
    }
}
