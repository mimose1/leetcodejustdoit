package leetcode.simple;

import leetcode.common.TreeNode;

/**
 * @Description
 *给定一个二叉树和一个目标和，判断该树中是否存在根节点到叶子节点的路径，这条路径上所有节点值相加等于目标和。
 *
 * 说明: 叶子节点是指没有子节点的节点。
 *
 * Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up all the values along the path equals the given sum.
 *
 * Note: A leaf is a node with no children.
 * @Author ccy
 * @Date 2019/5/12 8:44
 */
public class Code112PathSum {
    public boolean hasPathSum(TreeNode root, int sum) {
        int cur = 0;
        return pathSum(root,cur,sum);
    }

    private boolean pathSum(TreeNode root, int cur, int sum) {
        if(root==null) return false;
        cur += root.val;
        if(root.left==null&&root.right==null){
            return cur==sum;
        }
        return pathSum(root.left,cur,sum)||pathSum(root.right, cur, sum);
    }

    public static void main(String[] args) {
        /*TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2,3,4);
        root.right = new TreeNode(2,4,3);
        root.right.left = new TreeNode(4,2,1);*/
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        System.out.println((new Code112PathSum()).hasPathSum(root, 3));
    }
}
