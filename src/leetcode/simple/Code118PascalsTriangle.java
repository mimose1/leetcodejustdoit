package leetcode.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 *给定一个非负整数 numRows，生成杨辉三角的前 numRows 行。
 * 在杨辉三角中，每个数是它左上方和右上方的数的和。
 *
 * Given a non-negative integer numRows, generate the first numRows of Pascal's triangle.
 *
 * In Pascal's triangle, each number is the sum of the two numbers directly above it.
 * Example:
 *
 * Input: 5
 * Output:
 * [
 *      [1],
 *     [1,1],
 *    [1,2,1],
 *   [1,3,3,1],
 *  [1,4,6,4,1]
 * ]
 * @Author ccy
 * @Date 2019/5/12 16:44
 */
public class Code118PascalsTriangle {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> triangle = new ArrayList<>(numRows);
        for(int i=0; i<numRows; i++){
            List<Integer> num = new ArrayList<>(i+1);
            num.add(0,1);
            for(int j=1;j<=i;j++){
                num.add(j,(triangle.get(i-1).get(j-1))+(triangle.get(i-1).size()<(j+1)?0:triangle.get(i-1).get(j)));
            }
            triangle.add(num);
        }
        return triangle;
    }

    public static void main(String[] args) {
        System.out.println((new Code118PascalsTriangle()).generate(5));
    }
}
