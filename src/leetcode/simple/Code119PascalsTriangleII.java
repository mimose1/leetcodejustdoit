package leetcode.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * 给定一个非负索引 k，其中 k ≤ 33，返回杨辉三角的第 k 行。
 *
 * Given a non-negative index k where k ≤ 33, return the kth index row of the Pascal's triangle.
 *
 * Note that the row index starts from 0.
 * @Author ccy
 * @Date 2019/5/12 16:44
 */
public class Code119PascalsTriangleII {
    /**
     * 使用杨辉三角的公式，获取某一行
     * @param rowIndex
     * @return
     */
    public List<Integer> getRow(int rowIndex) {
        List<Integer> res = new ArrayList<>(rowIndex+1);
        long cur = 1;
        for(int i=0;i<=rowIndex;i++){
            res.add((int)cur);
            cur = cur*(rowIndex-i)/(i+1);
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println((new Code119PascalsTriangleII()).getRow(5));
    }
}
