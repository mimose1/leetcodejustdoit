package leetcode.simple;

/**
 * @Description
 * 给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。
 *
 * 如果你最多只允许完成一笔交易（即买入和卖出一支股票），设计一个算法来计算你所能获取的最大利润。
 *
 * 注意你不能在买入股票前卖出股票。
 *
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.
 *
 * Note that you cannot sell a stock before you buy one.
 * @Author ccy
 * @Date 2019/5/15 16:39
 */
public class Code121BestTimeToBuyAndSellStock {

    public int maxProfit(int[] prices) {
        int min = Integer.MAX_VALUE;
        int max = 0;
        for(int i=0;i<prices.length;i++){
            if(prices[i]<min) min = prices[i];
            else if(prices[i]-min>max){
                max = prices[i]-min;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        System.out.println((new Code121BestTimeToBuyAndSellStock()).maxProfit(new int[]{7,1,5,4,6,3}));
    }

}
