package leetcode.simple;

/**
 * @Description
 * 给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。
 *
 * 说明：本题中，我们将空字符串定义为有效的回文串。
 *
 * Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.
 *
 * Note: For the purpose of this problem, we define empty string as valid palindrome.
 * @Author ccy
 * @Date 2019/5/16 17:44
 */
public class Code125ValidPalindrome {
    public boolean isPalindrome(String s) {
        if(s==null) return false;
        s = s.toLowerCase();
        char[] ss = s.toCharArray();
        for(int i=0, j=ss.length-1; i<j;){
            char c1 = ss[i],c2 = ss[j];
            if((c1>='a'&&c1<='z')||(c1>='0'&&c1<='9')){
                c1 += ' ';
            }else{
                i++;
                continue;
            }
            if((c2>='a'&&c2<='z')||(c2>='0'&&c2<='9')){
                c2 += ' ';
            }else{
                j--;
                continue;
            }
            if(c1!=c2) return false;
            i++;
            j--;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println((new Code125ValidPalindrome()).isPalindrome("A man, a plan, a canal: Panama"));
    }
}
