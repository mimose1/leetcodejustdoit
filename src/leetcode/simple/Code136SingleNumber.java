package leetcode.simple;

/**
 * @Description
 *给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
 *
 * 说明：
 *
 * 你的算法应该具有线性时间复杂度。 你可以不使用额外空间来实现吗？
 *
 * Given a non-empty array of integers, every element appears twice except for one. Find that single one.
 *
 * Note:
 *
 * Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
 * @Author ccy
 * @Date 2019/5/18 10:04
 */
public class Code136SingleNumber {

    public int singleNumber(int[] nums) {
        //异或算法 由于重复的是一对的 而且异或算法中0^n=n,n^n=0
        //那么如果有个数组是{4,1,2,1,2}-----> 4^1^2^1^2 -------> (1^1)^(2^2)^4 -------> 0^0^4 ---> 4，就能获得唯一的不重复值
        int num = 0;
        if(nums.length==0) return nums[0];
        for (int i = 0; i < nums.length; i++) {
            num ^= nums[i];
        }
        return num;
    }

    public static void main(String[] args) {
        System.out.println((new Code136SingleNumber()).singleNumber(new int[]{4,1,2,1,2}));
    }
}
