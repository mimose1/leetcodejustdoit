package leetcode.simple;

import leetcode.common.ListNode;

import java.util.List;

/**
 * @Description
 * 给定一个链表，判断链表中是否有环。
 *
 * 为了表示给定链表中的环，我们使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。 如果 pos 是 -1，则在该链表中没有环。
 *
 * Given a linked list, determine if it has a cycle in it.
 *
 * To represent a cycle in the given linked list, we use an integer pos which represents the position (0-indexed) in the linked list where tail connects to. If pos is -1, then there is no cycle in the linked list.
 * @Author ccy
 * @Date 2019/5/18 10:19
 */
public class Code141LinkedListCycle {

    public boolean hasCycle(ListNode head) {
        //双指针，快慢指针，只要存在环形，慢指针肯定会追上快指针
        if(head==null||head.next==null) return false;
        ListNode slow = head,fast = head.next;
        while(slow!=fast){
            if(fast==null||fast.next==null) return false;
            slow = slow.next;//一次只走一步
            fast = fast.next.next;//一次走两步
        }
        return true;
    }

}
