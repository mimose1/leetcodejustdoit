package leetcode.simple;

import java.util.Stack;

/**
 * @Description
 * 设计一个支持 push，pop，top 操作，并能在常数时间内检索到最小元素的栈。
 *
 * push(x) -- 将元素 x 推入栈中。
 * pop() -- 删除栈顶的元素。
 * top() -- 获取栈顶元素。
 * getMin() -- 检索栈中的最小元素。
 *
 * Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.
 *
 * push(x) -- Push element x onto stack.
 * pop() -- Removes the element on top of the stack.
 * top() -- Get the top element.
 * getMin() -- Retrieve the minimum element in the stack.
 * @Author ccy
 * @Date 2019/5/18 10:46
 */
public class Code155MinStack {

    class MinStack {

        Stack<Integer> s;
        Stack<Integer> min;

        /** initialize your data structure here. */
        public MinStack() {
            s=new Stack<>();
            min = new Stack<>();
        }

        public void push(int x) {
            s.push(x);
            if(min.size()==0||min.peek()>=x){
                min.push(x);
            }

        }

        public void pop() {
            if(min!=null&&s.peek().equals(min.peek())){
                min.pop();
            }
            s.pop();
        }

        public int top() {
            return s.peek();
        }

        public int getMin() {
            return min.peek();
        }
    }

}
