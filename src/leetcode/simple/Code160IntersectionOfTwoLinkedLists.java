package leetcode.simple;

import leetcode.common.ListNode;

/**
 * @Description
 * 编写一个程序，找到两个单链表相交的起始节点。
 *
 * Write a program to find the node at which the intersection of two singly linked lists begins.
 *
 * @Author ccy
 * @Date 2019/5/18 11:14
 */
public class Code160IntersectionOfTwoLinkedLists {

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if(headA==null||headB==null) return null;
        ListNode tempA = headA;
        ListNode tempB = headB;
        //可以将这道题转换为141的双指针
        //如果A与B的长度不一样(假设A比B短)，则在循环后，A.next为空时，将其指向B的头部，此时A将类似于快指针，等到B.next为空指向A的头部时，A已经与B处在同一个起点了，此时循环将获得交点
        //如果A与B的长度一样，则在一次循环内，将得到交点
        //如果没有交点，最终将都为null，返回
        while(tempA!=tempB){//不是交点时，继续循环
            if(tempA==null){
                tempA = headB;//指向headB
            }else{
                tempA = tempA.next;
            }
            if(tempB==null){
                tempB = headA;//指向headA
            }else{
                tempB = tempB.next;
            }
        }
        return tempA;
    }

    public static void main(String[] args) {
        ListNode common = new ListNode(new int[]{8,4,5});
        ListNode headA = new ListNode(new int[]{4,1});
        headA.next = common;
        ListNode headB = new ListNode(new int[]{5,0,1});
        headB.next = common;
        System.out.println((new Code160IntersectionOfTwoLinkedLists()).getIntersectionNode(headA, headB));
    }
}
