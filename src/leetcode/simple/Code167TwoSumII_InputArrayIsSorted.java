package leetcode.simple;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * 给定一个已按照升序排列 的有序数组，找到两个数使得它们相加之和等于目标数。
 *
 * 函数应该返回这两个下标值 index1 和 index2，其中 index1 必须小于 index2。
 *
 * 说明:
 *
 * 返回的下标值（index1 和 index2）不是从零开始的。
 * 你可以假设每个输入只对应唯一的答案，而且你不可以重复使用相同的元素。
 *
 *
 * Given an array of integers that is already sorted in ascending order, find two numbers such that they add up to a specific target number.
 *
 * The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2.
 *
 * Note:
 *
 * Your returned answers (both index1 and index2) are not zero-based.
 * You may assume that each input would have exactly one solution and you may not use the same element twice.
 * @Author ccy
 * @Date 2019/5/19 9:40
 */
public class Code167TwoSumII_InputArrayIsSorted {
    public int[] twoSum(int[] numbers, int target) {
        if(numbers==null||numbers.length==0) return null;
        int left = 0,right = numbers.length-1;
        while(true){
            int num = numbers[left]+numbers[right];
            if(num==target) break;
            //因为是排序了的，所以可以根据计算值进行判断
            if(num<target) left++;
            if(num>target) right--;
        }
        return new int[]{left+1,right+1};
    }

    public static void main(String[] args) {
        int[] numbers = new int[]{2,7,11,15};
        int target = 9;
        int[] result = (new Code167TwoSumII_InputArrayIsSorted()).twoSum(numbers, target);
        if(result!=null){
            System.out.println("index:"+result[0]+":"+result[1]+"->"+numbers[result[0]]+"+"+numbers[result[1]]+"="+target);
        }
    }
}
