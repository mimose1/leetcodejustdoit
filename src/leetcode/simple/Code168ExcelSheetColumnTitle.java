package leetcode.simple;

/**
 * @Description
 * 给定一个正整数，返回它在 Excel 表中相对应的列名称。
 *
 * 例如，
 *
 * Given a positive integer, return its corresponding column title as appear in an Excel sheet.
 *
 * For example:
 *
 *     1 -> A
 *     2 -> B
 *     3 -> C
 *     ...
 *     26 -> Z
 *     27 -> AA
 *     28 -> AB
 * @Author ccy
 * @Date 2019/5/19 10:12
 */
public class Code168ExcelSheetColumnTitle {
    public String convertToTitle(int n) {
        StringBuilder sb = new StringBuilder();
        while(n>0){
            n-=1;
            sb.insert(0,(char)(n%26 + 'A'));
            n/=26;
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println((new Code168ExcelSheetColumnTitle()).convertToTitle(25));
    }
}
