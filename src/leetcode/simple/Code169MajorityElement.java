package leetcode.simple;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * 给定一个大小为 n 的数组，找到其中的众数。众数是指在数组中出现次数大于 ⌊ n/2 ⌋ 的元素。
 *
 * 你可以假设数组是非空的，并且给定的数组总是存在众数。
 *
 * Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.
 *
 * You may assume that the array is non-empty and the majority element always exist in the array.
 * @Author ccy
 * @Date 2019/5/20 9:18
 */
public class Code169MajorityElement {

    public int majorityElement(int[] nums) {
        //基于摩尔投票
        int count = 0;
        int current = 0;
        for(int num : nums){
            if(count==0){
                current = num;
            }
            if(current==num){
                count++;
            }else{
                count--;
            }
        }
        return current;
    }

    public static void main(String[] args) {
        System.out.println((new Code169MajorityElement()).majorityElement(new int[]{2,3,3,4,3,3,6,3}));
    }
}
