package leetcode.simple;

/**
 * @Description
 * 给定一个Excel表格中的列名称，返回其相应的列序号。
 *
 * 例如，
 *
 *
 * Given a column title as appear in an Excel sheet, return its corresponding column number.
 *
 * For example:
 *
 *     A -> 1
 *     B -> 2
 *     C -> 3
 *     ...
 *     Z -> 26
 *     AA -> 27
 *     AB -> 28
 * @Author ccy
 * @Date 2019/5/20 9:42
 */
public class Code171ExcelSheetColumnNumber {
    public int titleToNumber(String s) {
        if(s==null||s.length()<=0) return 0;
        char[] ss = s.toCharArray();
        int sum = 0;
        for (int i= ss.length-1; i>=0; i--){
            sum += (ss[i]-64)*Math.pow(26, (ss.length-i-1));
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println((new Code171ExcelSheetColumnNumber()).titleToNumber("ABCD"));
    }
}
