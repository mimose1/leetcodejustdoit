package leetcode.simple;

/**
 * @Description
 * 给定一个整数 n，返回 n! 结果尾数中零的数量。
 * 说明: 你算法的时间复杂度应为 O(log n) 。
 *
 * Given an integer n, return the number of trailing zeroes in n!.
 * Note: Your solution should be in logarithmic time complexity.
 * @Author ccy
 * @Date 2019/5/20 10:17
 */
public class Code172FactorialTrailingZeroes {

    public int trailingZeroes(int n) {
        //阶乘中寻找0的个数，实际上就是选择出现10的情况的次数，也就是2*5这种匹配的个数
        //由于一个数拆成2和5的情况，2的个数肯定是多于5的个数的，所以一共会有多少个匹配，取决于有多少个5
        //所以需要寻找5的个数，即n/5，但当n>25的时候，n/5的结果可能是还是大于等于5的，如5、10、15
        //这时候则需要继续n/5/5，直到最后n小于5或为0，才能知道一共会有多少个5，也就是出现多少次10，尾数有多少个0
        return n>=5?n/5+trailingZeroes(n/5):0;
    }

    public static void main(String[] args) {
        System.out.println((new Code172FactorialTrailingZeroes()).trailingZeroes(20));
    }
}
