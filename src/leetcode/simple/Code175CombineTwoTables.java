package leetcode.simple;

/**
 * @Description
 *
 *
 * Table: Person
 * 表1: Person
 * +-------------+---------+
 * | Column Name | Type    |
 * +-------------+---------+
 * | PersonId    | int     |
 * | FirstName   | varchar |
 * | LastName    | varchar |
 * +-------------+---------+
 * PersonId is the primary key column for this table.
 * PersonId 是上表主键
 *
 * Table: Address
 * 表2: Address
 * +-------------+---------+
 * | Column Name | Type    |
 * +-------------+---------+
 * | AddressId   | int     |
 * | PersonId    | int     |
 * | City        | varchar |
 * | State       | varchar |
 * +-------------+---------+
 * AddressId is the primary key column for this table.
 * AddressId 是上表主键
 *
 * Write a SQL query for a report that provides the following information for each person in the Person table, regardless if there is an address for each of those people:
 * 编写一个 SQL 查询，满足条件：无论 person 是否有地址信息，都需要基于上述两表提供 person 的以下信息：
 *
 * FirstName, LastName, City, State
 * @Author ccy
 * @Date 2019/5/20 14:19
 */
public class Code175CombineTwoTables {

    public static void main(String[] args) {
        String sql = "select a.FirstName,a.LastName,b.City,b.State from Person a left join Address b on a.PersonId = b.PersonId";
        System.out.println(sql);
    }
}
