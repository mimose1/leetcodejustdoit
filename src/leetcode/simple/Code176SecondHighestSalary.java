package leetcode.simple;

/**
 * @Description
 * 编写一个 SQL 查询，获取 Employee 表中第二高的薪水（Salary） 。
 *
 * Write a SQL query to get the second highest salary from the Employee table.
 * @Author ccy
 * @Date 2019/5/20 14:28
 */
public class Code176SecondHighestSalary {

    public static void main(String[] args) {
        String sql = "select min(salary) SecondHighestSalary" +
                "from(select salary,row_number()over(order by salary desc) rn" +
                "    from(" +
                "         select distinct salary from Employee" +
                "        )" +
                "    )" +
                "where rn!=1 and rn<=2;";
        System.out.println(sql);
    }
}
