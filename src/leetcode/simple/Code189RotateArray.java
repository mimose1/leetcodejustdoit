package leetcode.simple;

import java.util.Arrays;

/**
 * @Description
 * 给定一个数组，将数组中的元素向右移动 k 个位置，其中 k 是非负数。
 *
 *
 * Given an array, rotate the array to the right by k steps, where k is non-negative.
 * @Author ccy
 * @Date 2019/5/21 10:56
 */
public class Code189RotateArray {

    /**
     * rotate_1
     * @param nums
     * @param k
     */
    public void rotate(int[] nums, int k) {
        if(nums==null||nums.length==1||nums.length==k) return;
        int length = nums.length;
        k %= length;
        int[] temp1 = Arrays.copyOfRange(nums, length-k, length);//把会被推到前面的数组先存起来，到时候直接放到数组前面
        int[] temp2 = Arrays.copyOfRange(nums, 0, length-k);
        for(int i=temp2.length-1;i>=0;i--){
            nums[i+k] = temp2[i];
        }
        for(int i=0;i<temp1.length;i++){
            nums[i] = temp1[i];
        }
    }

    /**
     * rotate_2
     * @param nums
     * @param k
     */
    public void rotate_2(int[] nums, int k){
        if(nums==null||nums.length==1||nums.length==k) return;
        int length = nums.length;
        k %= length;
        r(nums,0,length-1);//先将数组全部翻转
        r(nums,0,k-1);//将数组左边部分翻转，获得被推到前面的数组
        r(nums,k,length-1);//将数组右边部分翻转，获得被推到后面的数组
    }
    private void r(int[] nums, int s, int e) {
        while(s<e){
            int temp = nums[s];
            nums[s++] = nums[e];
            nums[e--] = temp;
        }
    }

    //另外可以暴力法，直接一个个转换

    public static void main(String[] args) {
        int[] nums = new int[]{1,2,3,4,5,6,7};
        (new Code189RotateArray()).rotate(nums, 3);
        System.out.println(nums);
    }
}
