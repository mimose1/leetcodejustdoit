package leetcode.simple;

/**
 * @Description
 * 颠倒给定的 32 位无符号整数的二进制位。
 *
 * 提示：
 *
 * 请注意，在某些语言（如 Java）中，没有无符号整数类型。在这种情况下，输入和输出都将被指定为有符号整数类型，并且不应影响您的实现，因为无论整数是有符号的还是无符号的，其内部的二进制表示形式都是相同的。
 * 在 Java 中，编译器使用二进制补码记法来表示有符号整数。因此，在上面的 示例 2 中，输入表示有符号整数 -3，输出表示有符号整数 -1073741825。
 *
 *
 * Reverse bits of a given 32 bits unsigned integer.
 *
 * Note:
 *
 * Note that in some languages such as Java, there is no unsigned integer type. In this case, both input and output will be given as signed integer type and should not affect your implementation, as the internal binary representation of the integer is the same whether it is signed or unsigned.
 * In Java, the compiler represents the signed integers using 2's complement notation. Therefore, in Example 2 above the input represents the signed integer -3 and the output represents the signed integer -1073741825.
 * @Author ccy
 * @Date 2019/5/21 14:48
 */
public class Code190ReverseBits {

    public int reverseBits(int n) {
        int ret = 0;
        for(int i=0;i<32;i++) {
            ret <<= 1;
            ret |= n & 1;
            n >>=  1;
        }
        return ret;
    }

    public static void main(String[] args) {
        System.out.println((new Code190ReverseBits()).reverseBits(43261596));
    }
}
