package leetcode.simple;

/**
 * @Description
 * 编写一个函数，输入是一个无符号整数，返回其二进制表达式中数字位数为 ‘1’ 的个数（也被称为汉明重量）。
 *
 * Write a function that takes an unsigned integer and return the number of '1' bits it has (also known as the Hamming weight).
 * @Author ccy
 * @Date 2019/5/21 15:17
 */
public class Code191NumberOf1Bits {

    public int hammingWeight(int n) {
        int count = 0;
        while(n != 0) {
            n = n & (n-1);
            count++;
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println((new Code191NumberOf1Bits()).hammingWeight(3));
    }
}
