package leetcode.simple;

/**
 * @Description  动态规划：http://www.sohu.com/a/153858619_466939
 * 你是一个专业的小偷，计划偷窃沿街的房屋。每间房内都藏有一定的现金，影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。
 *
 * 给定一个代表每个房屋存放金额的非负整数数组，计算你在不触动警报装置的情况下，能够偷窃到的最高金额。
 *
 *
 * You are a professional robber planning to rob houses along a street. Each house has a certain amount of money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have security system connected and it will automatically contact the police if two adjacent houses were broken into on the same night.
 *
 * Given a list of non-negative integers representing the amount of money of each house, determine the maximum amount of money you can rob tonight without alerting the police.
 * @Author ccy
 * @Date 2019/5/21 15:27
 */
public class Code198HouseRobber {

    //动态规划算法
    //拆分：总房间数n，如果想获取第n间房间的最大收益，则需要根据第n-2的最大收益加上自己的收益，同时还要判断第n-1的最大收益，是否大于前面计算的最大收益
    //     若想获取第n-1的最大收益，则需要根据n-1-2的最大收益加上自己的收益，同时还要判断第n-2-1的最大收益，是否大于前面计算的最大收益.....
    //     直到不可拆分的最小解，也就是第1间的最大收益，同时也可以得到第2间的最大收益，则从该最小解（第二间）开始往后面推算，计算最终的最大收益
    public int rob(int[] nums) {
        if(nums==null||nums.length==0) return 0;
        if(nums.length==1) return nums[0];
        int n = nums.length;
        int[] dp = new int[n];
        dp[0] = nums[0];//第一间房间的最大金额
        dp[1] = Math.max(dp[0],nums[1]);//第二间房间的最大金额
        for(int i=2;i<n;i++){
            //如果该房间的金额加上最近的房间的金额低于相邻房间的最大金额，那就不偷这一间，进入下一间继续判断
            //也就是，dp[i]代表了从第一间到第i+1间房间，能获得的最大收益
            dp[i] = Math.max(dp[i-1], dp[i-2]+nums[i]);
        }
        return dp[n-1];
    }

    public static void main(String[] args) {
        System.out.println((new Code198HouseRobber()).rob(new int[]{1,2}));
    }
}
