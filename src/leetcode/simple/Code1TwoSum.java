package leetcode.simple;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
 * 你可以假设每种输入只会对应一个答案。但是，你不能重复利用这个数组中同样的元素。
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * @Author ccy
 * @Date 2019/5/5 10:20
 */
public class Code1TwoSum {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer,Integer> map = new HashMap<>();
        for(int i=0,length=nums.length; i<length; i++){
            int r = target-nums[i];
            if(map.containsKey(r)){
                return new int[]{map.get(r),i};
            }
            map.put(nums[i],i);
        }
        throw new IllegalArgumentException("not solution");
    }

    public static void main(String[] args) {
        int[] nums = new int[]{2, 7, 11, 15};
        int target = 9;
        int[] result = (new Code1TwoSum()).twoSum(nums, target);
        System.out.println("done");
    }
}
