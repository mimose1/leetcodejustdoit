package leetcode.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * 编写一个算法来判断一个数是不是“快乐数”。
 *
 * 一个“快乐数”定义为：对于一个正整数，每一次将该数替换为它每个位置上的数字的平方和，然后重复这个过程直到这个数变为 1，也可能是无限循环但始终变不到 1。如果可以变为 1，那么这个数就是快乐数。
 *
 * Write an algorithm to determine if a number is "happy".
 *
 * A happy number is a number defined by the following process: Starting with any positive integer, replace the number by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers.
 * @Author ccy
 * @Date 2019/5/21 18:06
 */
public class Code202HappyNumber {

    //不是快乐数的数称为不快乐数(unhappy number)，所有不快乐数的数位平方和计算，最后都会进入 4 → 16 → 37 → 58 → 89 → 145 → 42 → 20 → 4 的循环中
    public boolean isHappy(int n) {
        //不使用说给上面那个规律，直接判断计算后的值是否在之前已经出现过了，如果出现过了，那就还是会进行这个循环，肯定不是happy number
        List<Integer> list = new ArrayList<>();
        while(true){
            int ret = 0;
            while(n>0){
                int temp = n%10;
                ret += temp*temp;
                n /= 10;
            }
            if(ret==1) return true;
            if(list.contains(ret)){
                return false;
            }
            n = ret;
            list.add(ret);
        }
    }

    public static void main(String[] args) {
        System.out.println((new Code202HappyNumber()).isHappy(19));
    }
}
