package leetcode.simple;

import leetcode.common.ListNode;

/**
 * @Description
 * 删除链表中等于给定值 val 的所有节点。
 *
 * Remove all elements from a linked list of integers that have value val.
 * @Author ccy
 * @Date 2019/5/22 10:42
 */
public class Code203RemoveLinkedListElements {

    //通过前驱的方式进行删除
    public ListNode removeElements(ListNode head, int val) {
        if(head==null) return head;
        ListNode head2 = new ListNode(0);
        head2.next = head;
        ListNode cur = head2;
        while(cur.next!=null){
            if(cur.next.val==val){
                cur.next = cur.next.next;
            }else{
                cur = cur.next;
            }
        }
        return head2.next;
    }

    //通过遍历的方式进行删除
    public ListNode removeElements2(ListNode head, int val){
        if(head==null) return head;
        head.next = removeElements2(head.next, val);
        return head.val==val?head.next:head;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(new int[]{1,2,3,4,5,6,7});
        System.out.println((new Code203RemoveLinkedListElements()).removeElements2(head, 1));
    }
}
