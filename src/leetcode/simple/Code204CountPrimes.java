package leetcode.simple;

/**
 * @Description
 * 统计所有小于非负整数 n 的质数的数量。
 *
 *
 * Count the number of prime numbers less than a non-negative number, n.
 * @Author ccy
 * @Date 2019/5/22 10:57
 */
public class Code204CountPrimes {

    //第一种，对于一个自然数N，不需要判断从[2,N-1]的所有数，只需要判断 小于等于根号N的所有数，能否除，就能判断是否为质数
    public int countPrimes(int n) {
        if(n<3) return 0;
        int count = 1;//从大于3开始算，2默认为一个质数
        for(int i=n-1;i>=3;i--){
            if(i%2==0)
                continue;
            int num = (int) Math.round(Math.sqrt(i));
            boolean flag = false;
            for(int j=3;j<=num;j+=2){
                if(i%j==0){
                    flag = true;
                    break;
                }
            }
            if(!flag)
                count++;
        }
        return count;
    }

    //第二种，厄拉多塞筛法
    public int countPrimes2(int n) {
        boolean[] count = new boolean[n];
        if (n > 0){
            count[0] = false;
        }
        if (n >1){
            count[1] = false;
        }
        for(int i = 2; i<n;i++){
            count[i] = true;
        }
        for(int i=2;i*i < n; i++){//该算法通过倍数来计算，那么就只需要查询2到根号n之间的数，再计算其倍数，是不是质数就可以了
            if(count[i]){
                for(int j=i*i;j<n;j+=i){//计算i的倍数，如2的话，就计算4、6、8... 是不是质数
                    count[j]=false;
                }
            }
        }
        int res = 0;
        for(boolean c : count){
            res += c?1:0;
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println((new Code204CountPrimes()).countPrimes2(50));
    }
}
