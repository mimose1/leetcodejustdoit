package leetcode.simple;

import com.sun.org.apache.bcel.internal.classfile.Code;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * 给定两个字符串 s 和 t，判断它们是否是同构的。
 *
 * 如果 s 中的字符可以被替换得到 t ，那么这两个字符串是同构的。
 *
 * 所有出现的字符都必须用另一个字符替换，同时保留字符的顺序。两个字符不能映射到同一个字符上，但字符可以映射自己本身。
 * 说明:
 * 你可以假设 s 和 t 具有相同的长度。
 *
 *
 * Given two strings s and t, determine if they are isomorphic.
 *
 * Two strings are isomorphic if the characters in s can be replaced to get t.
 *
 * All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character but a character may map to itself.
 * Note:
 * You may assume both s and t have the same length.
 * @Author ccy
 * @Date 2019/5/22 14:02
 */
public class Code205IsomorphicStrings {

    /**
     * funtion_1
     * @param s
     * @param t
     * @return
     */
    public boolean isIsomorphic(String s, String t) {
        if(s.length()==0&&t.length()==0) return true;
        Map<Character,Character> map = new HashMap<>();
        for(int i=0;i<s.length();i++){
            if(map.containsKey(s.charAt(i))){
                if(map.get(s.charAt(i))!=t.charAt(i)){
                    return false;
                }
            }else{
                if(map.containsValue(t.charAt(i))){
                    return false;
                }
                //关键在这个put
                map.put(s.charAt(i), t.charAt(i));
            }
        }
        return true;
    }

    /**
     * funtion_2
     * @param s
     * @param t
     * @return
     */
    public boolean isIsomorphic2(String s, String t) {
        if(s.length()==0&&t.length()==0) return true;
        for(int i=0;i<s.length();i++){
            //如果字符的下一个出现的索引不一样的话，说明不是同构
            if(s.indexOf(s.charAt(i),i+1)!=t.indexOf(t.charAt(i),i+1)){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println((new Code205IsomorphicStrings()).isIsomorphic2("paple", "title"));
    }
}
