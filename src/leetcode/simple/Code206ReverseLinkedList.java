package leetcode.simple;

import leetcode.common.ListNode;

/**
 * @Description
 * 反转一个单链表。
 *
 * 进阶:
 * 你可以迭代或递归地反转链表。你能否用两种方法解决这道题？
 *
 * Reverse a singly linked list.
 *
 * A linked list can be reversed either iteratively or recursively. Could you implement both?
 * @Author ccy
 * @Date 2019/5/22 14:43
 */
public class Code206ReverseLinkedList {

    public ListNode reverseList(ListNode head) {
        if(head==null) return head;
        ListNode curr = head;
        ListNode prev = null;
        while(curr!=null){
            ListNode temp = curr.next;
            curr.next = prev;
            prev = curr;
            curr = temp;
        }
        return prev;
    }

    /**
     * 递归
     * @param head
     * @return
     */
    public ListNode reverseList2(ListNode head) {
        if (head == null || head.next == null)
            return head;
        ListNode p = reverseList2(head.next);
        head.next.next = head;
        head.next = null;
        return p;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(new int[]{1,23,4,5,6,7});
        System.out.println((new Code206ReverseLinkedList()).reverseList2(head));
    }
}
