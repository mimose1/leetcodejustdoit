package leetcode.simple;

import java.util.Stack;

/**
 * @Description
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
 * 有效字符串需满足：
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 * 注意空字符串可被认为是有效字符串。
 * Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
 * An input string is valid if:
 * Open brackets must be closed by the same type of brackets.
 * Open brackets must be closed in the correct order.
 * Note that an empty string is also considered valid.
 * @Author ccy
 * @Date 2019/5/5 10:32
 */
public class Code20ValidParentheses {
    public boolean isValid(String s) {
        char[] ss = s.toCharArray();
        Stack<Character> stack = new Stack<>();
        for(char val : ss){
            if(stack.size()==0) stack.push(val);
            else{
                char c1 = stack.peek();
                if((c1=='('&&val==')')||(c1=='{'&&val=='}')||(c1=='['&&val==']'))
                    stack.pop();
                else
                    stack.push(val);
            }
        }
        if(stack.size()==0)
            return true;
        return false;
    }

    public static void main(String[] args) {
        System.out.println((new Code20ValidParentheses()).isValid("((({{}})))"));
    }
}
