package leetcode.simple;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Description
 * 给定一个整数数组，判断是否存在重复元素。
 *
 * 如果任何值在数组中出现至少两次，函数返回 true。如果数组中每个元素都不相同，则返回 false。
 *
 * Given an array of integers, find if the array contains any duplicates.
 *
 * Your function should return true if any value appears at least twice in the array, and it should return false if every element is distinct.
 * @Author ccy
 * @Date 2019/5/22 15:15
 */
public class Code217ContainsDuplicate {

    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for(int i=0;i<nums.length;i++){
            set.add(nums[i]);
        }
        if(set.size()!=nums.length){
            return true;
        }
        return false;
    }

    public boolean containsDuplicate2(int[] nums){
        if(nums.length<=1) return false;
        Arrays.sort(nums);
        int length = nums.length;
        for(int i=0;i<length-1;i++){
            if(nums[i]==nums[i+1]) return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println((new Code217ContainsDuplicate()).containsDuplicate2(new int[]{1,2,3,1}));
    }
}
