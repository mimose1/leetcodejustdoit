package leetcode.simple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * 给定一个整数数组和一个整数 k，判断数组中是否存在两个不同的索引 i 和 j，使得 nums [i] = nums [j]，并且 i 和 j 的差的绝对值最大为 k。
 *
 * Given an array of integers and an integer k, find out whether there are two distinct indices i and j in the array such that nums[i] = nums[j] and the absolute difference between i and j is at most k.
 * @Author ccy
 * @Date 2019/5/22 15:34
 */
public class Code219ContainsDuplicateII {

    public boolean containsNearbyDuplicate(int[] nums, int k) {
        if(nums.length<=1) return false;
        Map<Integer,Integer> map = new HashMap<>();
        for(int i=0,length=nums.length;i<length;i++){
            if(map.containsKey(nums[i])){
                if(Math.abs(i-map.get(nums[i]))<=k){
                    return true;
                }
            }
            map.put(nums[i], i);
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(new Code219ContainsDuplicateII().containsNearbyDuplicate(new int[]{99,99}, 2));
    }
}
