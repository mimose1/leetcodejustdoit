package leetcode.simple;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @Description
 * 使用队列实现栈的下列操作：
 *
 * push(x) -- 元素 x 入栈
 * pop() -- 移除栈顶元素
 * top() -- 获取栈顶元素
 * empty() -- 返回栈是否为空
 * 注意:
 *
 * 你只能使用队列的基本操作-- 也就是 push to back, peek/pop from front, size, 和 is empty 这些操作是合法的。
 * 你所使用的语言也许不支持队列。 你可以使用 list 或者 deque（双端队列）来模拟一个队列 , 只要是标准的队列操作即可。
 * 你可以假设所有操作都是有效的（例如, 对一个空的栈不会调用 pop 或者 top 操作）。
 *
 *
 * Implement the following operations of a stack using queues.
 *
 * push(x) -- Push element x onto stack.
 * pop() -- Removes the element on top of the stack.
 * top() -- Get the top element.
 * empty() -- Return whether the stack is empty.
 * Notes:
 *
 * You must use only standard operations of a queue -- which means only push to back, peek/pop from front, size, and is empty operations are valid.
 * Depending on your language, queue may not be supported natively. You may simulate a queue by using a list or deque (double-ended queue), as long as you use only standard operations of a queue.
 * You may assume that all operations are valid (for example, no pop or top operations will be called on an empty stack).
 * @Author ccy
 * @Date 2019/5/22 15:50
 */
public class Code225ImplementStackUsingQueues {

    //队列：先进先出
    //栈：  先进后出
    class MyStack {

        Queue<Integer> q1;
        Queue<Integer> q2;

        /** Initialize your data structure here. */
        public MyStack() {
            //利用两个队列来实现，q1作为插入使用，倒置插入到q2，q2作为最终结果
            q1 = new LinkedList<>();
            q2 = new LinkedList<>();
        }

        /** Push element x onto stack. */
        public void push(int x) {
            q1.offer(x);
            while(!q2.isEmpty()){
                q1.offer(q2.poll());
            }
            Queue temp = q1;
            q1 = q2;
            q2 = temp;
        }

        /** Removes the element on top of the stack and returns that element. */
        public int pop() {
            return q2.poll();
        }

        /** Get the top element. */
        public int top() {
            return q2.peek();
        }

        /** Returns whether the stack is empty. */
        public boolean empty() {
            return q2.isEmpty();
        }
    }


    public static void main(String[] args) {
        MyStack stack = (new Code225ImplementStackUsingQueues()).new MyStack();
        System.out.println(stack.empty());
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.push(6);
        System.out.println(stack.pop());
        stack.push(7);
        System.out.println(stack.top());
        System.out.println(stack.empty());
    }
}
