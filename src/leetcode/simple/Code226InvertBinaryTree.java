package leetcode.simple;

import leetcode.common.TreeNode;

/**
 * @Description
 * 翻转一棵二叉树。
 * Invert a binary tree.
 *
 * 示例：
 *
 * 输入：
 *
 *      4
 *    /   \
 *   2     7
 *  / \   / \
 * 1   3 6   9
 * 输出：
 *
 *      4
 *    /   \
 *   7     2
 *  / \   / \
 * 9   6 3   1
 *
 * @Author ccy
 * @Date 2019/5/27 14:19
 */
public class Code226InvertBinaryTree {

    public TreeNode invertTree(TreeNode root) {
        if(root==null||(root.left==null&&root.right==null)) return root;
        TreeNode left = invertTree(root.left);
        TreeNode right = invertTree(root.right);
        root.left = right;
        root.right = left;
        return root;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        TreeNode left = new TreeNode(2,1,3);
        TreeNode right = new TreeNode(7,6,9);
        root.left = left;
        root.right = right;
        System.out.println((new Code226InvertBinaryTree()).invertTree(root));
    }

}
