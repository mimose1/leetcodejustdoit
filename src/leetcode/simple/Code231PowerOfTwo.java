package leetcode.simple;

/**
 * @Description
 * 给定一个整数，编写一个函数来判断它是否是 2 的幂次方。
 *
 * Given an integer, write a function to determine if it is a power of two.
 *
 * @Author ccy
 * @Date 2019/5/27 14:33
 */
public class Code231PowerOfTwo {

    public boolean isPowerOfTwo(int n) {
        return n>0&&(n&(n-1))==0;
    }

    public static void main(String[] args) {
        System.out.println((new Code231PowerOfTwo()).isPowerOfTwo(16));
    }
}
