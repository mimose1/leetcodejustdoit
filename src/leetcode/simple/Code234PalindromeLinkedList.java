package leetcode.simple;

import leetcode.common.ListNode;

/**
 * @Description
 * 请判断一个链表是否为回文链表。
 *
 * 进阶：
 * 你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？
 *
 * Given a singly linked list, determine if it is a palindrome.
 *
 * Follow up:
 * Could you do it in O(n) time and O(1) space?
 *
 * @Author ccy
 * @Date 2019/5/27 15:08
 */
public class Code234PalindromeLinkedList {

    //使用快慢指针，fast将比slow快一倍，代表着当fast到达终点的时候，slow将在中间的地方
    //同时倒置node，这代表着当fast到达终点的时候，前半部分的ListNode将被倒置
    //最后比较前半部分和后半部分，一样的话就是回文链表
    public boolean isPalindrome(ListNode head) {
        if(head==null||head.next==null) return true;
        ListNode slow = head,fast = head;
        ListNode temp = null;
        ListNode last = null;
        while(fast.next!=null&&fast.next.next!=null){
            temp = slow;
            slow = slow.next;
            fast = fast.next.next;
            if(last==null){
                last = head;
                last.next = null;
            }else{
                temp.next = last;
                last = temp;
            }
        }
        if(fast.next!=null){
            fast = slow.next;
            slow.next = temp;
        }else{
            fast = slow.next;
            slow = temp;
        }
        while(slow!=null||fast!=null){
            if(slow==null||fast==null||slow.val!=fast.val){
                return false;
            }
            slow = slow.next;
            fast = fast.next;
        }
        return true;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(new int[]{1,2,3,4,5,5,4,3,2,1});
        System.out.println((new Code234PalindromeLinkedList()).isPalindrome(head));
    }
}
