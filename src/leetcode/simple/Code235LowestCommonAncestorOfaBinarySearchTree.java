package leetcode.simple;

import leetcode.common.TreeNode;

/**
 * @Description
 * 给定一个二叉搜索树, 找到该树中两个指定节点的最近公共祖先。
 *
 * 百度百科中最近公共祖先的定义为：“对于有根树 T 的两个结点 p、q，最近公共祖点也可以是它自己的祖先）。”
 *
 * 例如，给定如下二叉搜索树:  root = [6,2,8,0,4,7,9,null,null,3,5]
 *
 *
 * Given a binary search tree (BST), find the lowest common ancestor (LCA) of two given nodes in the BST.
 *
 * According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes p and q as the lowest node in T that has both p and q as descendants (where we allow a node to be a descendant of itself).”
 *
 * Given binary search tree:  root = [6,2,8,0,4,7,9,null,null,3,5]
 *
 * @Author ccy
 * @Date 2019/6/10 9:27
 */
public class Code235LowestCommonAncestorOfaBinarySearchTree {

    /**
     * 二叉排序树的特点是 若它的左子树不空，则左子树上所有结点的值均小于它的根结点的值； 若它的右子树不空，则右子树上所有结点的值均大于它的根结点的值； 它的左、右子树也分别为二叉排序树
     * 那么公共祖先的值肯定是介于p和q的值的
     * @param root
     * @param p
     * @param q
     * @return
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if(root==null) return root;
        if(p.val>q.val){
            TreeNode temp = p;
            p = q;
            q = temp;
        }
        if(root.val>=p.val&&root.val<=q.val){
            return root;
        }else if(root.val<p.val&&root.val<q.val){
            return lowestCommonAncestor(root.right, p, q);
        }else {
            return lowestCommonAncestor(root.left, p, q);
        }
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(6);
        TreeNode left = new TreeNode(2, 0, 4);
        TreeNode right = new TreeNode(8, 7, 9);
        root.left = left;
        root.right = right;
        TreeNode p = new TreeNode(8);
        TreeNode q = new TreeNode(2);
        System.out.println((new Code235LowestCommonAncestorOfaBinarySearchTree()).lowestCommonAncestor(root, p, q).val);
    }

}
