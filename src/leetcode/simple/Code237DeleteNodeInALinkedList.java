package leetcode.simple;

import leetcode.common.ListNode;

/**
 * @Description
 * 请编写一个函数，使其可以删除某个链表中给定的（非末尾）节点，你将只被给定要求被删除的节点。
 *
 * Write a function to delete a node (except the tail) in a singly linked list, given only access to that node.
 *
 * @Author ccy
 * @Date 2019/6/10 10:17
 */
public class Code237DeleteNodeInALinkedList {

    //这道题的意思是，给定的这个node就是要删除的那个节点，那么就相当于，只需要将该节点的值改为下个节点的值，就可以跳过该节点，相当于删除
    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(4);
        ListNode node = new ListNode(new int[]{5,1,9});
        head.next = node;
        (new Code237DeleteNodeInALinkedList()).deleteNode(node);
        System.out.println(head.toString());
    }
}
