package leetcode.simple;

import java.lang.reflect.Array;
import java.util.*;

/**
 * @Description
 * 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
 *
 * 示例 1:
 *
 * 输入: s = "anagram", t = "nagaram"
 * 输出: true
 * 示例 2:
 *
 * 输入: s = "rat", t = "car"
 * 输出: false
 * 说明:
 * 你可以假设字符串只包含小写字母。
 *
 * 进阶:
 * 如果输入字符串包含 unicode 字符怎么办？你能否调整你的解法来应对这种情况？
 *
 *
 * Given two strings s and t , write a function to determine if t is an anagram of s.
 *
 * Example 1:
 *
 * Input: s = "anagram", t = "nagaram"
 * Output: true
 * Example 2:
 *
 * Input: s = "rat", t = "car"
 * Output: false
 * Note:
 * You may assume the string contains only lowercase alphabets.
 *
 * Follow up:
 * What if the inputs contain unicode characters? How would you adapt your solution to such case?
 *
 * @Author ccy
 * @Date 2019/6/10 13:57
 */
public class Code242ValidAnagram {

    public boolean isAnagram(String s, String t) {
        if(s.length()!=t.length()) return false;
        Map<Character,Integer> map = new HashMap<>();
        char[] chars = s.toCharArray();
        char[] chart = t.toCharArray();
        for(char ss : chars){
            map.put(ss, (map.containsKey(ss)?map.get(ss):0)+1);
        }
        for(char ts : chart){
            if(!map.containsKey(ts)){
                return false;
            }else if(map.get(ts)==1){
                map.remove(ts);
            }else{
                map.put(ts, map.get(ts)-1);
            }
        }
        if(map.size()==0){
            return true;
        }
        return false;
    }

    //试了下 好像更慢了...
    public boolean isAnagram2(String s, String t) {
        List<String> ss = Arrays.asList(s.split(""));
        List<String> ts = Arrays.asList(t.split(""));
        Collections.sort(ss);
        Collections.sort(ts);
        if(ss.toString().equals(ts.toString())){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println((new Code242ValidAnagram()).isAnagram2("anagram", "nagaram"));
    }

}
