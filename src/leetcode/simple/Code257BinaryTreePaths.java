package leetcode.simple;

import leetcode.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * Given a binary tree, return all root-to-leaf paths.
 *
 * Note: A leaf is a node with no children.
 *
 *
 * 给定一个二叉树，返回所有从根节点到叶子节点的路径。
 *
 * 说明: 叶子节点是指没有子节点的节点。
 * @Author ccy
 * @Date 2019/6/10 14:22
 */
public class Code257BinaryTreePaths {
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> ret = new ArrayList<>();
        if(root==null) return ret;
        paths(ret,"",root);
        return ret;
    }

    private void paths(List<String> ret, String sb, TreeNode root) {
        if(root==null){
            return;
        }
        sb += root.val;
        if(root.left==null&&root.right==null){
            ret.add(sb);
            return;
        }
        paths(ret,sb + "->" ,root.left);
        paths(ret,sb + "->",root.right);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode left = new TreeNode(2);
        left.right = new TreeNode(5);
        TreeNode right = new TreeNode(3);
        root.left = left;
        root.right = right;
        System.out.println((new Code257BinaryTreePaths()).binaryTreePaths(root));
    }
}
