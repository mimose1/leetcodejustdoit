package leetcode.simple;

/**
 * @Description
 * 给定一个非负整数 num，反复将各个位上的数字相加，直到结果为一位数。
 *
 * 示例:
 *
 * 输入: 38
 * 输出: 2
 * 解释: 各位相加的过程为：3 + 8 = 11, 1 + 1 = 2。 由于 2 是一位数，所以返回 2。
 * 进阶:
 * 你可以不使用循环或者递归，且在 O(1) 时间复杂度内解决这个问题吗？
 *
 *
 * Given a non-negative integer num, repeatedly add all its digits until the result has only one digit.
 *
 * Example:
 *
 * Input: 38
 * Output: 2
 * Explanation: The process is like: 3 + 8 = 11, 1 + 1 = 2.
 *              Since 2 has only one digit, return it.
 * Follow up:
 * Could you do it without any loop/recursion in O(1) runtime?
 * @Author ccy
 * @Date 2019/6/11 17:41
 */
public class Code258AddDigits {

    //暴力法
    public int addDigits(int num) {
        if(num<=9) return num;
        int sum = 0;
        while(num>9){
            sum = 0;
            while(num>=1){
                sum += num%10;
                num /= 10;
            }
            num = sum;
        }
        return sum;
    }

    //其实存在一个规律，即与9相关，9除余后，余数为结果值，若为0，则结果值为9
    //假设一个三位数，abc，原先的sum1为 100*a+10*b+c   逐个相加的sum2为 a+b+c
    //两者的差为 sum1-sum2 = 99a+99b，与9相关
    public int addDigits2(int num){
        if(num<=9) return num;
        int ret = num%9;
        return ret==0?9:ret;
    }

    public static void main(String[] args) {
        System.out.println((new Code258AddDigits()).addDigits2(38));
    }
}
