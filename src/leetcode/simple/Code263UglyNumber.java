package leetcode.simple;

/**
 * @Description
 * 编写一个程序判断给定的数是否为丑数。
 *
 * 丑数就是只包含质因数 2, 3, 5 的正整数。
 *
 *
 * Write a program to check whether a given number is an ugly number.
 *
 * Ugly numbers are positive numbers whose prime factors only include 2, 3, 5.
 *
 * @Author ccy
 * @Date 2019/6/12 20:54
 */
public class Code263UglyNumber {
    public boolean isUgly(int num) {
        if(num == 0) return false;
        if(num == 1) return true;
        num = num%2==0?num/2:num%3==0?num/3:num%5==0?num/5:0;
        return isUgly(num);
    }

    public static void main(String[] args) {
        System.out.println((new Code263UglyNumber()).isUgly(38));
    }
}
