package leetcode.simple;

/**
 * @Description
 * 给定一个包含 0, 1, 2, ..., n 中 n 个数的序列，找出 0 .. n 中没有出现在序列中的那个数。
 * 说明:
 * 你的算法应具有线性时间复杂度。你能否仅使用额外常数空间来实现?
 *
 * Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one that is missing from the array.
 * Note:
 * Your algorithm should run in linear runtime complexity. Could you implement it using only constant extra space complexity?
 *
 * @Author ccy
 * @Date 2019/6/12 20:56
 */
public class Code268MissingNumber {
    public int missingNumber(int[] nums) {
        int sum = nums.length * (nums.length + 1) / 2;
        int sum2 = 0;
        for(int num : nums){
            sum2 += num;
        }
        return sum - sum2;
    }

    public static void main(String[] args) {
        System.out.println((new Code268MissingNumber()).missingNumber(new int[]{0,1,2,3,4,5,6,7,9,10}));
    }
}
