package leetcode.simple;

/**
 * @Description
 *给定一个排序数组，你需要在原地删除重复出现的元素，使得每个元素只出现一次，返回移除后数组的新长度。
 * 不要使用额外的数组空间，你必须在原地修改输入数组并在使用 O(1) 额外空间的条件下完成。
 * Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.
 * Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
 * @Author ccy
 * @Date 2019/5/5 10:48
 */
public class Code26RemoveDuplicatesFromSortedArray {

    public int removeDuplicates(int[] nums) {
        //使用指针
        if(nums==null) return 0;
        if(nums.length<=1) return nums.length;
        int pos = 0;
        for(int i=1,length=nums.length;i<length;i++){
            //如果不重复，则将指针往前移动
            if(nums[pos] != nums[i]){
                pos++;
                nums[pos] = nums[i];
            }
        }
        return pos+1;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{1,1,2};
        int result = (new Code26RemoveDuplicatesFromSortedArray()).removeDuplicates(nums);
        System.out.println(result);
    }
}
