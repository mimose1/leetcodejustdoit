package leetcode.simple;

/**
 * @Description
 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
 *
 *
 * Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.
 *
 * @Author ccy
 * @Date 2019/7/1 17:21
 */
public class Code283MoveZeroes {

    /**
     * 使用一个指针index，先指向初始位置0，循环数组，当不为0的时候，调换两个的位置，并把指针指向下一个
     * @param nums
     */
    public void moveZeroes(int[] nums) {
        int index = 0;
        for(int i=0,length=nums.length;i<length;i++){
            if(nums[i]!=0){
                int temp = nums[index];
                nums[index++] = nums[i];
                nums[i] = temp;
            }
        }
    }

    public static void main(String[] args) {
        int[] nums = new int[]{0,0,1,2,3,4,0,9};
        (new Code283MoveZeroes()).moveZeroes(nums);
        System.out.println(nums);
    }


}
