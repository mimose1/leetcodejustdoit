package leetcode.simple;

/**
 * @Description
 * 实现 strStr() 函数。
 * 给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从0开始)。如果不存在，则返回  -1。
 * Implement strStr().
 * Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
 * @Author ccy
 * @Date 2019/5/5 10:53
 */
public class Code28ImplementStrStr {
    public int strStr(String haystack, String needle) {
        if(needle==null||"".equals(needle)){
            return 0;
        }
        char[] hayStacks = haystack.toCharArray();
        char[] needles = needle.toCharArray();
        int haylength = hayStacks.length;
        int neelength = needles.length;
        int search = haylength-neelength;
        for(int i=0;i<=search;i++){
            if(hayStacks[i]!=needles[0]){
                continue;
            }
            int j=1;
            for(j=1;j<needles.length&&hayStacks[i+j]==needles[j];j++){

            }
            if(j==neelength){
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println((new Code28ImplementStrStr()).strStr("hello", "ll"));
    }
}
