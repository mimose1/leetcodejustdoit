package leetcode.simple;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * 给定一种规律 pattern 和一个字符串 str ，判断 str 是否遵循相同的规律。
 *
 * 这里的 遵循 指完全匹配，例如， pattern 里的每个字母和字符串 str 中的每个非空单词之间存在着双向连接的对应规律。
 * 示例1:
 *
 * 输入: pattern = "abba", str = "dog cat cat dog"
 * 输出: true
 * 示例 2:
 *
 * 输入:pattern = "abba", str = "dog cat cat fish"
 * 输出: false
 * 示例 3:
 *
 * 输入: pattern = "aaaa", str = "dog cat cat dog"
 * 输出: false
 * 示例 4:
 *
 * 输入: pattern = "abba", str = "dog dog dog dog"
 * 输出: false
 * 说明:
 * 你可以假设 pattern 只包含小写字母， str 包含了由单个空格分隔的小写字母。    
 *
 *
 * Given a pattern and a string str, find if str follows the same pattern.
 *
 * Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in str.
 *
 * @Author ccy
 * @Date 2019/7/2 10:19
 */
public class Code290WordPattern {
    public boolean wordPattern(String pattern, String str) {
        String[] strs = str.split(" ");
        char[] patterns = pattern.toCharArray();
        if(strs.length!=patterns.length){
            return false;
        }
        Map<Character,String> map = new HashMap<Character, String>();
        for(int i=0,length=patterns.length;i<length;i++){
            if(map.containsKey(patterns[i])){
                if(!map.get(patterns[i]).equals(strs[i])){
                    return false;
                }
            }else{
                if(map.containsValue(strs[i])){
                    return false;
                }
            }
            map.put(patterns[i], strs[i]);
        }
        return true;
     }

    public static void main(String[] args) {
        System.out.println((new Code290WordPattern()).wordPattern("abba", "dog you you dog"));
    }
}
