package simple;

/**
 * @Description
 * 给定一个整数数组  nums，求出数组从索引 i 到 j  (i ≤ j) 范围内元素的总和，包含 i,  j 两点。
 *
 * 示例：
 *
 * 给定 nums = [-2, 0, 3, -5, 2, -1]，求和函数为 sumRange()
 *
 * sumRange(0, 2) -> 1
 * sumRange(2, 5) -> -1
 * sumRange(0, 5) -> -3
 * 说明:
 *
 * 你可以假设数组不可变。
 * 会多次调用 sumRange 方法。
 *
 *
 * Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.
 *
 * Example:
 * Given nums = [-2, 0, 3, -5, 2, -1]
 *
 * sumRange(0, 2) -> 1
 * sumRange(2, 5) -> -1
 * sumRange(0, 5) -> -3
 * Note:
 * You may assume that the array does not change.
 * There are many calls to sumRange function.
 *
 * @Author ccy
 * @Date 2019/7/2 11:17
 */
public class Code303RangeSumQueryImmutable {

    public static void main(String[] args) {
        NumArray numArray = (new Code303RangeSumQueryImmutable()).new NumArray(new int[]{-2,0,3,-5,2,-1});
        System.out.println(numArray.sumRange(0, 2));
    }

    class NumArray{
        private int[] sum;

        /**
         * 提前把每一步的计算结果存储到缓存中，计算的时候，直接相减就可以得到两个index之间的sum值
         * @param nums
         */
        public NumArray(int[] nums) {
            sum = new int[nums.length+1];
            for(int i=0,length=nums.length;i<length;i++){
                sum[i+1] = sum[i] + nums[i];
            }
        }

        public int sumRange(int i, int j) {
            return sum[j+1] - sum[i];
        }
    }
}
