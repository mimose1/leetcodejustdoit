package simple;

/**
 * @Description
 * 给定一个整数，写一个函数来判断它是否是 3 的幂次方。
 *
 * Given an integer, write a function to determine if it is a power of three.
 * @Author ccy
 * @Date 2019/7/2 11:31
 */
public class Code326PowerOfThree {
    public boolean isPowerOfThree(int n) {
        if(n<=0){
            return false;
        }
        while(n%3==0){
            n /= 3;
        }
        if(n==1){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println((new Code326PowerOfThree()).isPowerOfThree(16));
    }
}
