package leetcode.simple;

/**
 * @Description
 * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
 * 你可以假设数组中无重复元素。
 * Given a sorted array and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.
 * You may assume no duplicates in the array.
 * @Author ccy
 * @Date 2019/5/5 10:01
 */
public class Code35SearchInsertPosition {
    public int searchInsert(int[] nums, int target) {
        for(int i=0;i<nums.length;i++){
            if(nums[i]>=target){
                return i;
            }
        }
        return nums.length;
    }

    //The second method--use dichotomy
    public int searchInsert2(int[] nums, int target){
        int left = 0,right = nums.length-1,mid = 0;
        while(left<=right){
            mid = (left+right)/2;
            if(nums[mid]<target){
                left = mid+1;
            }else if(nums[mid]>target){
                right = mid-1;
            }else{
                return mid;
            }
        }
        return left;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{1,3,5,6};
        int target = 3;
//        System.out.println((new Code35SearchInsertPosition()).searchInsert(nums, target));
        System.out.println((new Code35SearchInsertPosition()).searchInsert2(nums, target));
    }
}
