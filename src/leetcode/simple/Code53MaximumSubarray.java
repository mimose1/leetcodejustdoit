package leetcode.simple;

/**
 * @Description
 * 给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
 * Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.
 * @Author ccy
 * @Date 2019/5/5 14:56
 */
public class Code53MaximumSubarray {
    public int maxSubArray(int[] nums) {
        int max = nums[0];
        int add = 0;
        for(int i=0; i<nums.length; i++){
            add+=nums[i];
            max = Math.max(add, max);
            if(add<0){
                add = 0;
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{-2,1,-3,4,-1,2,1,-5,4};
        System.out.println((new Code53MaximumSubarray()).maxSubArray(nums));
    }
}
