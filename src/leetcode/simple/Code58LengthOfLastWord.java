package leetcode.simple;

/**
 * @Description
 * 给定一个仅包含大小写字母和空格 ' ' 的字符串，返回其最后一个单词的长度。
 *
 * 如果不存在最后一个单词，请返回 0 。
 *
 * 说明：一个单词是指由字母组成，但不包含任何空格的字符串。
 *
 *
 * Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the length of last word in the string.
 *
 * If the last word does not exist, return 0.
 *
 * Note: A word is defined as a character sequence consists of non-space characters only.
 * @Author ccy
 * @Date 2019/5/5 15:47
 */
public class Code58LengthOfLastWord {
    public int lengthOfLastWord(String s) {
//        s = s.trim();
//        String[] ss = s.split(" ");
//        int max = ss[ss.length-1].length();
//        return max;

        //other way
        char[] chars = s.toCharArray();
        char space = ' ';
        int end=chars.length-1;
        while(end>=0&&chars[end]==space){
            end--;
        }
        int start = end;
        while(start>=0&&chars[start]!=space){
            start--;
        }
        return end-start;
    }

    public static void main(String[] args) {
        System.out.println((new Code58LengthOfLastWord()).lengthOfLastWord("Today is a nice day"));
    }
}
