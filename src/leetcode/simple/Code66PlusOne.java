package leetcode.simple;

/**
 * @Description
 * 给定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。
 *
 * 最高位数字存放在数组的首位， 数组中每个元素只存储一个数字。
 *
 * 你可以假设除了整数 0 之外，这个整数不会以零开头。
 *
 *
 * Given a non-empty array of digits representing a non-negative integer, plus one to the integer.
 *
 * The digits are stored such that the most significant digit is at the head of the list, and each element in the array contain a single digit.
 *
 * You may assume the integer does not contain any leading zero, except the number 0 itself.
 * @Author ccy
 * @Date 2019/5/5 16:06
 */
public class Code66PlusOne {

    public int[] plusOne(int[] digits) {
        int carry = 1;
        for(int i=digits.length-1;i>=0;i--){
            int dig = digits[i]+carry;
            digits[i] = dig%10;
            carry = dig/10;
        }
        if(carry>0){
            int[] res = new int[digits.length+1];
            res[0] = carry;
            return res;
        }
        return digits;
    }

    public static void main(String[] args) {
        int[] digits = {9,9,9};
        int[] res = (new Code66PlusOne()).plusOne(digits);
        for(int r : res){
            System.out.print(r+"->");
        }
    }
}
