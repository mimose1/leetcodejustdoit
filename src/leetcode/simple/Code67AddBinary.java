package leetcode.simple;

/**
 * @Description
 * 给定两个二进制字符串，返回他们的和（用二进制表示）。
 *
 * 输入为非空字符串且只包含数字 1 和 0。
 *
 * Given two binary strings, return their sum (also a binary string).
 *
 * The input strings are both non-empty and contains only characters 1 or 0.
 * @Author ccy
 * @Date 2019/5/5 17:43
 */
public class Code67AddBinary {
    public String addBinary(String a, String b) {
        int i = a.length(),j=b.length();
        int length = i>j?i:j;
        int diff = Math.max(length-i, length-j);
        StringBuilder diffSb = new StringBuilder();
        for(int d=0;d<diff;d++){
            diffSb.append("0");
        }
        if(i>j) b = diffSb.append(b).toString();
        if(j>i) a = diffSb.append(a).toString();
        String[] as = a.split("");
        String[] bs = b.split("");
        StringBuilder sb = new StringBuilder();
        int carry = 0;
        for(int m=length-1; m>=0; m--){
            int sum = Integer.parseInt(as[m])+Integer.parseInt(bs[m])+carry;
            carry = sum/2;
            sb.insert(0, sum%2);
        }
        if(carry==1){
            sb.insert(0, carry);
        }
        return sb.toString();
    }

    /**
     * 不补位 直接操作
     * @param a
     * @param b
     * @return
     */
    public String addBinary2(String a, String b) {
        StringBuilder sb = new StringBuilder();
        char[] as = a.toCharArray();
        char[] bs = b.toCharArray();
        int alength = a.length()-1;
        int blength = b.length()-1;
        int carry = 0;
        while (alength>=0||blength>=0){
            int aint = 0,bint = 0;
            if(alength>=0) aint = as[alength]-'0';
            if(blength>=0) bint = bs[blength]-'0';
            sb.insert(0, (aint+bint+carry)%2);
            carry = (aint+bint+carry)/2;
            alength--;
            blength--;
        }
        if(carry>0) sb.insert(0, carry);
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println((new Code67AddBinary()).addBinary2("11", "1"));
    }
}
