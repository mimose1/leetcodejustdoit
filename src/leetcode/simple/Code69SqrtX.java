package leetcode.simple;

/**
 * @Description
 *实现 int sqrt(int x) 函数。
 *
 * 计算并返回 x 的平方根，其中 x 是非负整数。
 *
 * 由于返回类型是整数，结果只保留整数的部分，小数部分将被舍去。
 *
 * Implement int sqrt(int x).
 *
 * Compute and return the square root of x, where x is guaranteed to be a non-negative integer.
 *
 * Since the return type is an integer, the decimal digits are truncated and only the integer part of the result is returned.
 * @Author ccy
 * @Date 2019/5/6 9:41
 */
public class Code69SqrtX {
    public int mySqrt(int x) {
        //二分法 从0 到 x/2+1 的范围内
        long left = 0;
        long right = x/2+1;
        while(left<=right){
            long mid = (left + right) / 2;
            if(mid*mid == x) return (int) mid;
            else if(mid*mid < x) left = mid + 1;
            else right = mid - 1;
        }
        return (int) right;
    }

    public int mySqrt2(int x){
        //牛顿迭代法
        if(x<=1) return x;
        long res = x;
        while(res>x/res){
            res = (res+x/res)/2;
        }
        return (int) res;
    }

    public static void main(String[] args) {
        System.out.println((new Code69SqrtX()).mySqrt2(785412));
    }
}
