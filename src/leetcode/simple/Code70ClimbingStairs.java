package leetcode.simple;

/**
 * @Description
 *假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
 *
 * 每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
 *
 * 注意：给定 n 是一个正整数。
 *
 * You are climbing a stair case. It takes n steps to reach to the top.
 *
 * Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
 *
 * Note: Given n will be a positive integer.
 * @Author ccy
 * @Date 2019/5/6 10:13
 */
public class Code70ClimbingStairs {

    public int climbStairs(int n) {
        //规律是斐波那契数列
        //n=2: 2; n=3: 3; n=4: 5; n=5: 8
        //用尾递归 + 斐波那契数列
        return f(n,1,1);
    }

    private int f(int n, int f1, int f2){
        if(n==0){
            return f1;
        }
        //前两项之和为该项的值
        return f(n-1, f2, f1+f2);
    }

    public static void main(String[] args) {
        System.out.println((new Code70ClimbingStairs()).climbStairs(5));
    }
}
