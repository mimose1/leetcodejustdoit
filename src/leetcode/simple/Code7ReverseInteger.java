package leetcode.simple;

/**
 * @Description
 * 给出一个 32 位的有符号整数，你需要将这个整数中每位上的数字进行反转。
 * Given a 32-bit signed integer, reverse digits of an integer.
 * @Author ccy
 * @Date 2019/5/5 10:25
 */
public class Code7ReverseInteger {
    public int reverse(int x) {
        int res = 0;
        while(x!=0){
            int y = x%10;
            x = x/10;
            if(res > Integer.MAX_VALUE/10){
                return 0;
            }else if(res < Integer.MIN_VALUE/10){
                return 0;
            }
            res = res*10 + y;
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println((new Code7ReverseInteger()).reverse(123));
    }
}
