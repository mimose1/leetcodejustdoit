package leetcode.simple;

import leetcode.common.ListNode;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * 给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。
 * Given a sorted linked list, delete all duplicates such that each element appear only once.
 * @Author ccy
 * @Date 2019/5/6 10:32
 */
public class Code83RemoveDuplicatesFromSortedList {

    /**
     * 由于已排序，则直接判断即可
     * @param head
     * @return
     */
    public ListNode deleteDuplicates(ListNode head) {
        ListNode curr = head;
        while(curr!=null&&curr.next!=null){
            if(curr.val == curr.next.val){
                curr.next = curr.next.next;
            }else{
                curr = curr.next;
            }
        }
        return head;
    }

    /**
     * 如果链表不是排序链表的话，可用这个来进行去重
     * @param head
     * @return
     */
    public ListNode deleteDuplicates2(ListNode head) {
        if(head==null||head.next==null) return head;
        Map<Integer,Integer> map = new HashMap<>();
        map.put(head.val, 1);
        ListNode curr = head;
        while(curr!=null&&curr.next!=null){
            if(map.containsKey(curr.next.val)){
                curr.next = curr.next.next;
            }else{
                map.put(curr.next.val, 1);
                curr = curr.next;
            }
        }
        return head;
    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(new int[]{1,2,1,2,3,3,4,5});
        System.out.println((new Code83RemoveDuplicatesFromSortedList()).deleteDuplicates2(l1));
    }
}
