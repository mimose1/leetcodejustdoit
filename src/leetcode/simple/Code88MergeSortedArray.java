package leetcode.simple;

/**
 * @Description
 * 给定两个有序整数数组 nums1 和 nums2，将 nums2 合并到 nums1 中，使得 num1 成为一个有序数组。
 *
 * 说明:
 *
 * 初始化 nums1 和 nums2 的元素数量分别为 m 和 n。
 * 你可以假设 nums1 有足够的空间（空间大小大于或等于 m + n）来保存 nums2 中的元素。
 *
 * Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.
 *
 * Note:
 *
 * The number of elements initialized in nums1 and nums2 are m and n respectively.
 * You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2.
 * @Author ccy
 * @Date 2019/5/6 10:54
 */
public class Code88MergeSortedArray {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int size = m+n-1;
        m--;
        n--;
        while(m>=0&&n>=0){
            nums1[size--] = nums1[m]>nums2[n]?nums1[m--]:nums2[n--];
        }
        while(m>=0){
            nums1[size--] = nums1[m--];
        }
        while(n>=0){
            nums1[size--] = nums2[n--];
        }
    }

    public static void main(String[] args) {
        int m = 3;
        int n = 3;
        int[] nums1 = new int[]{1,2,3,0,0,0};
        int[] nums2 = new int[]{2,5,6};
        (new Code88MergeSortedArray()).merge(nums1, m, nums2, n);
        System.out.println("done");
    }
}
