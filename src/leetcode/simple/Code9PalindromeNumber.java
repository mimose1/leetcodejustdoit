package leetcode.simple;

/**
 * @Description
 * 判断一个整数是否是回文数。回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。
 * Determine whether an integer is a palindrome. An integer is a palindrome when it reads the same backward as forward.
 * @Author ccy
 * @Date 2019/5/5 10:27
 */
public class Code9PalindromeNumber {
    public boolean isPalindrome(int x) {
        if(x<0) return false;
        if(x%10==0&&x!=0) return false;
        int res = 0;
        while(x>res){
            res = res*10 + x%10;
            x /=10;
        }

        return x==res||x==res/10;
    }

    public static void main(String[] args) {
        System.out.println((new Code9PalindromeNumber()).isPalindrome(121));
    }
}
